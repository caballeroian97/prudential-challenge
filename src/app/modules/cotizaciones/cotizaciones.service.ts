import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {CurrencyConvertion} from "../../core/CurrencyConvertion";

@Injectable({
  providedIn: 'root',
})
export class CotizacionesService {

  private readonly resourceUrl: string = 'https://api.frankfurter.app/latest';

  constructor(private http: HttpClient) {}

  public convert(from: string, to: string){
    const params = new HttpParams()
      .set('amount', '1')
      .set('from', from)
      .set('to', to);
    return this.http.get<CurrencyConvertion>(`${this.resourceUrl}`, { params })
  }

}
