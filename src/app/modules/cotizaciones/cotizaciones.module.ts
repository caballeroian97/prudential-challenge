import {NgModule} from "@angular/core";
import {CotizacionesComponent} from "./cotizaciones.component";
import {CotizacionesRoutingModule} from "./cotizaciones-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatSelectModule} from "@angular/material/select";
import {MatOptionModule} from "@angular/material/core";
import {CommonModule} from "@angular/common";
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
    CotizacionesComponent
  ],
  imports: [
    CotizacionesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatOptionModule,
    CommonModule,
    MatDialogModule,
    MatButtonModule,
  ],
})
export class CotizacionesModule {}
