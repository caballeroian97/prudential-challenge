import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CotizacionesService} from "./cotizaciones.service";
import {CurrencyConvertion} from "../../core/CurrencyConvertion";

@Component({
  selector: 'app-cotizaciones',
  templateUrl: './cotizaciones.component.html',
  styleUrls: ['./cotizaciones.component.css']
})
export class CotizacionesComponent implements OnInit {

  countryArray  = [
    "AUD",
    "BGN",
    "BRL",
    "CAD",
    "CHF",
    "CNY",
    "CZK","DKK",
    "EUR",
    "GBP",
    "HKD",
    "HUF",
    "IDR",
    "ILS",
    "INR",
    "ISK",
    "JPY",
    "KRW",
    "MXN",
    "MYR",
    "NOK",
    "NZD",
    "PHP",
    "PLN",
    "RON",
    "SEK",
    "SGD",
    "THB",
    "TRY",
    "USD",
    "ZAR"
  ]
  form: FormGroup;
  result: CurrencyConvertion | null = {};
  constructor(private fb: FormBuilder,
              private cotizacionesService: CotizacionesService) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm(){
    this.form = this.fb.group({
      from: ['', Validators.required],
      to: ['', Validators.required],
    })
  }

  submit() {
    if(this.getFormControl()['from'].value != '' && this.getFormControl()['to'].value != null && this.getFormControl()['from'].value === this.getFormControl()['to'].value){
      this.getFormControl()['to'].setErrors({'cannotBeEqual': true});
    } else {
      this.getFormControl()['to'].setErrors({'cannotBeEqual': null});
      this.getFormControl()['to'].updateValueAndValidity();
    }

    if(!this.form.valid) {
      return;
    }

    this.cotizacionesService.convert(this.form.value.from, this.form.value.to).subscribe(
      response => {
        this.result = response;
      },
      error => {
        //HANDLE ERRORS
      }
    )
  }

  getFormControl(){
    return this.form.controls;
  }

  clearResult() {
    this.result = null;
  }

  getAmount() {
    return this.result ? this.result.amount?.toString().concat(' ') : '';
  }

  getFromCurrency(){
      return this.result ? this.result.base?.concat(' ') : '';
  }

  getEquivalency() {
    if(this.result && this.form.value.to) {
      return 'equivale '.concat(this.result.rates?.[this.form.value?.to]);
    }

    return '';
  }

  getToCurrency() {
    if(this.result && this.form.value.to) {
      return this.form.value.to
    }

    return '';
  }
}
