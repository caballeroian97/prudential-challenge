export interface CurrencyConvertion {
  amount?: number,
  base?: string,
  date?: string,
  rates?: Data<any>
}

type Data<T extends Record<string, any>> = T;
